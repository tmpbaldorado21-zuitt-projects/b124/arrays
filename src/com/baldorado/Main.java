package com.baldorado;

import java.lang.management.GarbageCollectorMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
	    //Arrays

        //Syntax

        //dataType[] identifier = new dataType[arrayLength];

        int[] numbersList = new int[3];

        //Add new items in an array
        numbersList[0] = 10;
        numbersList[1] = 25;
        numbersList[2] = 40;

        System.out.println(numbersList[1]);

        System.out.println(Arrays.toString(numbersList)); // will display the memory reference of the value

        //Alternative way of creating a traditional array

        //Syntax
        //dataType[] identifier = { element,  [element, ...]};

        String[] surnamesList = { "Romeo" , "Cabagnot", "Perez"};

        System.out.println(surnamesList[0]);

        System.out.println(Arrays.toString(surnamesList));// will display the array as string

        //Multidimensional (nested) Array

        //Syntax
        //dataType[][] identifier = new dataType[arrayLength1][arrayLength2];

        String[][] classRoom = new String[3][3];

        //First Row
        classRoom[0][0] = "Athos";
        classRoom[0][1] = "Porthos";
        classRoom[0][2] = "Artemis";

        //Second Row
        classRoom[1][0] = "Mickey";
        classRoom[1][1] = "Donald";
        classRoom[1][2] = "Goofy";

        //Third Row
        classRoom[2][0] = "Harry";
        classRoom[2][1] = "Ron";
        classRoom[2][2] = "Hermione";

        System.out.println(Arrays.deepToString(classRoom));
        System.out.println(Arrays.deepToString(classRoom));

        //ArrayList --> dynamic

        //Syntax
        //ArrayList<dataType> identifier = new ArrayList<dataType()>;

        ArrayList<String> studentList = new ArrayList<String>();

        //Creating elements
        studentList.add("John");
        studentList.add("Paul");
        studentList.add("Matthew");
        // studentList.add("Jude"); //will dynamically adjust the storage allocation

        System.out.println(studentList);

        ArrayList<Integer> ageList = new ArrayList<Integer>();

        ageList.add(23);

        System.out.println(ageList);

        //Retrieving Elements

        System.out.println(studentList.get(0));
        System.out.println(studentList.get(2));

        //Updating elements

        System.out.println(studentList.get(1));
        studentList.set(1, "Peter");
        System.out.println(studentList.get(1));

        //Deleting elements
        studentList.remove(2);
        System.out.println(studentList);

        //Mini Activity:
        //1.
        System.out.println("Original Student " + studentList);
        studentList.clear();
        //2
        System.out.println(studentList);
        //3.
        System.out.println(studentList.size());

        //ArrayList with default values

        ArrayList<String> employeeList = new ArrayList<>(Arrays.asList("Giannis", "Chris", "Jrue" ));
        System.out.println(employeeList);

        //Hashmaps --> a collection of items with key-value pairs

        //Syntax
        //Hashmap<keyDataType, valueDataType> identifier = new HashMap<keyDataType, valueDataType>();

        HashMap<String, String> jobPositionsList = new HashMap<String, String>();

        //Creating Elements
        jobPositionsList.put("Giannis", "Power Forward");
        jobPositionsList.put("Chris", "Shooting Guard");
        jobPositionsList.put("Jrue", "Point Guard");

        System.out.println(jobPositionsList);

        //Retrieving Elements
        System.out.println(jobPositionsList.get("Giannis"));

        //Updating Elements
        jobPositionsList.replace("Jrue", "Score-First Guard");
        System.out.println(jobPositionsList);

        //Removing Elements
        jobPositionsList.remove("Chris");
        System.out.println(jobPositionsList);
        System.out.println(jobPositionsList.size());

        //Mini-Activity: Retrieve all hashmap keys
        System.out.println(jobPositionsList.keySet());

        //Mini-Activity:
        //Create a hashmap of grades list.
        HashMap<String, Integer> gradeList = new HashMap<String, Integer>();
        //The key is the lastname of a student, and grade is the value.
        //Create 3 mockup items for the list.
        gradeList.put("Baldorado", 89);
        gradeList.put("Del Mundo", 95);
        gradeList.put("Austria", 94);
        //Display the hashmap to the console.
        System.out.println(gradeList);
        System.out.println(gradeList.keySet());

    }
}
